import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torchvision.datasets as dset
import torchvision.transforms as T

from myOptimizer_conv import myOptimizer

from BinaryConv import BinaryConv2d
from sign import SignFunction

DSET_train = dset.MNIST('./datasets', train=True, download=True,
                           transform=T.Compose([
                               T.ToTensor(),
                               T.Normalize((0.1307,), (0.3081,))
                           ]))
loader_train = DataLoader(DSET_train, batch_size=64)

DSET_test = dset.MNIST('./datasets', train=False, download=True,
                          transform=T.Compose([
                              T.ToTensor(),
                              T.Normalize((0.1307,), (0.3081,))
                          ]))
loader_test = DataLoader(DSET_test, batch_size=1000)

# Sign nonlinearity
sign = SignFunction.apply

# Here's where we define the architecture of the model...
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = BinaryConv2d(1, 10, kernel_size=5, bias=False)
        self.conv2 = BinaryConv2d(10, 20, kernel_size=5, bias=False)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = sign(F.max_pool2d(self.conv1(x), 2))
        x = sign(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, 1)

model = Net()
params = list(model.named_parameters())

if torch.cuda.is_available():
    model = model.cuda()

# Constant to control how frequently we print train loss
print_every = 100

def train(epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(loader_train):
        if torch.cuda.is_available():
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()

        model.conv1.weight.data = model.conv1.weightOrg
        model.conv2.weight.data = model.conv2.weightOrg

        optimizer.step()
        if batch_idx % print_every == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.2f}\tDensity: {:.3f}'.format(
                epoch, batch_idx * len(data), len(loader_train.dataset),
                100. * batch_idx / len(loader_train), loss,
                float((torch.sum(model.conv2.weight.data != 0))) / (model.conv2.weight.data.shape[0] * model.conv2.weight.data.shape[1]
                                                                    * model.conv2.weight.data.shape[2] * model.conv2.weight.data.shape[3]) ))

def test():
    model.eval()
    test_loss = 0
    correct = 0
    for data, target in loader_test:
        if torch.cuda.is_available():
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)
        output = model(data)
        test_loss += F.nll_loss(output, target, size_average=False) # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    test_loss /= len(loader_test.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(loader_test.dataset),
        100. * correct / len(loader_test.dataset)))


decay_lr_every = 10
lr = 0.01
for epoch in range(1, 100 + 1):
    if epoch % decay_lr_every == 0:
        lr /= 2
    optimizer = myOptimizer(model.parameters(), lr=lr, momentum=0.5)
#    optimizer = optim.Adam(model.parameters(), lr=lr)

    train(epoch)
    test()