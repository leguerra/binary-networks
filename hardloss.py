import torch
from torch.autograd.function import InplaceFunction
from torch.nn.parameter import Variable

class HardlossFunction(InplaceFunction):

    @staticmethod
    def forward(ctx, input, target):
        ctx.save_for_backward(input, target)
        max_indexes = torch.max(input, 1)[1]
        diff = max_indexes - target
        temp = torch.sum(diff != 0)     # The loss is the number of missclassified images

        loss = torch.FloatTensor(1)
        loss[0] = temp
        return loss

    @staticmethod
    def backward(ctx, grad_output):
        # Use pdb debugger to debug the backwards
        # import pdb
        # pdb.set_trace()
        input, target = ctx.saved_variables

        grad_input = None
        grad_input = Variable(torch.zeros(input.size())).cuda()

        # Although these variables were torchTensors in the forward (this is automatically done by pytorch),
        # they are stored in ctx as pytorch Variables
        input = input.data
        target = target.data

        max_indexes = torch.max(input, 1)[1]

        if grad_input.size(0) == 1:
            if((max_indexes != target)[0]):
                grad_input.data[0][target] = -1
                grad_input.data[0][max_indexes] = 1
        elif grad_input.size(0) >= 2:
            k = 0
            for max_idx, t in zip(max_indexes, target):
                if((max_idx != t)):
                    grad_input.data[k][t] = -1
                    grad_input.data[k][max_idx] = 1
                k += 1

        return grad_input, None