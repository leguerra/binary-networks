import torch
from torch.autograd.function import InplaceFunction

# Inherit from Function
class SignFunction(InplaceFunction):
    # Note that both forward and backward are @staticmethods
    @staticmethod
    def forward(ctx, input):
        output = input
        output[input >= 0] = 1
        output[input < 0] = -1
        return output

    # This function has only a single output, so it gets only one gradient
    @staticmethod
    def backward(ctx, grad_output):
        grad_input = None
        grad_input = grad_output

        return grad_input