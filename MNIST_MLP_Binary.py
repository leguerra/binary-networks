# For reference:
# https://github.com/itayhubara/BinaryNet/blob/master/Models/BinaryNet_MNIST_Model.lua

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torchvision.datasets as dset
import torchvision.transforms as T
import matplotlib.pyplot as plt

from BinaryLinear import BinaryLinear
from sign import SignFunction
from myOptimizer_MLP import myOptimizer
from display import print_gradients, plot_weights

# Number of neurons in middle layer
NUM_HIDDEN = 2048/8

# Constant to control how frequently we print train loss
print_every = 100
plot = True
plot_every = 100

# Load dataset
cifar10_train = dset.MNIST('./cs231n/datasets', train=True, download=True,
                           transform=T.Compose([
                               T.ToTensor(),
                               T.Normalize((0.1307,), (0.3081,))
                           ]))
loader_train = DataLoader(cifar10_train, batch_size=64)

cifar10_test = dset.MNIST('./cs231n/datasets', train=False, download=True,
                          transform=T.Compose([
                              T.ToTensor(),
                              T.Normalize((0.1307,), (0.3081,))
                          ]))
loader_test = DataLoader(cifar10_test, batch_size=1000)

# Activation function
sign = SignFunction.apply

# Architecture of the model
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = BinaryLinear(784, NUM_HIDDEN, True)
        self.fc1_bn = nn.BatchNorm1d(NUM_HIDDEN)
#        self.fc2 = BinaryLinear(NUM_HIDDEN, NUM_HIDDEN, True)
#        self.fc2_bn = nn.BatchNorm1d(NUM_HIDDEN)
        self.fc3 = BinaryLinear(NUM_HIDDEN, NUM_HIDDEN, True)
        self.fc3_bn = nn.BatchNorm1d(NUM_HIDDEN)
        self.fc4 = BinaryLinear(NUM_HIDDEN, 10, True)

    def forward(self, x):
        x = x.view(-1, 784)
        x = self.fc1(x)
#        x = self.fc1_bn(x)
        x = sign(x)
#        x = self.fc2(x)
#        x = self.fc2_bn(x)
#        x = sign(x)
        x = self.fc3(x)
#        x = self.fc3_bn(x)
        x = sign(x)
        x = self.fc4(x)

        return F.log_softmax(x)


model = Net()
# model.fc4.register_backward_hook(print_gradients)
params = list(model.named_parameters())

if torch.cuda.is_available():
    model.cuda()

# Four axes, returned as a 2-d array
plt.ion()
fig, axarr = plt.subplots(2, 2)


def train(epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(loader_train):
        if torch.cuda.is_available():
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()

        if (batch_idx % plot_every == 0) and plot:
            model.cpu()
            plot_weights(model, axarr)
            model.cuda()

        optimizer.step()

        if batch_idx % print_every == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.2f}\tDensity: {:.3f}'.format(
                epoch, batch_idx * len(data), len(loader_train.dataset),
                100. * batch_idx / len(loader_train), loss.data[0],
                float((torch.sum(model.fc3.weight.data != 0))) / (model.fc3.weight.data.shape[0]*model.fc3.weight.data.shape[1]) ))

def test():
    model.eval()
    test_loss = 0
    correct = 0
    for data, target in loader_test:
        if torch.cuda.is_available():
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = model(data)
        test_loss += F.nll_loss(output, target, size_average=False).data[0] # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    test_loss /= len(loader_test.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(loader_test.dataset),
        100. * correct / len(loader_test.dataset)))


decay_lr_every = 10
lr = 14
for epoch in range(1, 1000 + 1):
    if epoch % decay_lr_every == 0:
        lr += 1
    optimizer = myOptimizer(model.parameters(), lr=lr, momentum=0.5)
#    optimizer = optim.Adam(model.parameters(), lr=lr)

    train(epoch)
    test()


