import torch
from torch.autograd.function import InplaceFunction

def mylinear(trainFlag, input, weight, bias=None):

    if input.dim() == 2 and bias is not None:
        # fused op is marginally faster
        return myaddmm.apply(bias, input, weight.t())

    output = input.matmul(weight.t())
    if bias is not None:
        output += bias
    return output

class myaddmm(InplaceFunction):

    @staticmethod
    def forward(ctx, add_matrix, matrix1, matrix2, alpha=1, beta=1, inplace=False):
        ctx.alpha = alpha
        ctx.beta = beta
        ctx.add_matrix_size = add_matrix.size()

        output = add_matrix.new().resize_as_(add_matrix)  # Originally _get_output(ctx, add_matrix, inplace=inplace)

        # Note: There is no need to binarize matrix2 on the backward function
		# because they'll be stored with the final value
        ctx.save_for_backward(matrix1, matrix2)

        matrix2 = binarize(matrix2)
        return torch.addmm(alpha, add_matrix, beta, matrix1, matrix2, out=output)

    @staticmethod
    def backward(ctx, grad_output):
        # import pdb
        # pdb.set_trace()

        matrix1, matrix2 = ctx.saved_variables
        grad_add_matrix = grad_matrix1 = grad_matrix2 = None

        if ctx.needs_input_grad[0]:
            grad_add_matrix = maybe_unexpand(grad_output, ctx.add_matrix_size)
            if ctx.alpha != 1:
                grad_add_matrix = grad_add_matrix.mul(ctx.alpha)

        if ctx.needs_input_grad[1]:
            if matrix1.stride() == (1, matrix1.size(0)):
                # column major gradient if input is column major
                grad_matrix1 = torch.mm(matrix2, grad_output.t()).t()
            else:
                grad_matrix1 = torch.mm(grad_output, matrix2.t())
            if ctx.beta != 1:
                grad_matrix1 *= ctx.beta

        if ctx.needs_input_grad[2]:
            if matrix2.stride() == (1, matrix2.size(0)):
                # column major gradient if input is column major
                grad_matrix2 = torch.mm(grad_output.t(), matrix1).t()
            else:
                grad_matrix2 = torch.mm(matrix1.t(), grad_output)
            if ctx.beta != 1:
                grad_matrix2 *= ctx.beta

        grad_add_matrix[:] = 0

        # return grad_bias,     grad_input,   grad_weigths, None, None, None
        return grad_add_matrix, grad_matrix1, grad_matrix2, None, None, None


def maybe_unexpand(variable, old_size):
    num_unsqueezed = variable.dim() - len(old_size)
    expanded_dims = [dim for dim, (expanded, original)
                     in enumerate(zip(variable.size()[num_unsqueezed:], old_size))
                     if expanded != original]

    for _ in range(num_unsqueezed):
        variable = variable.sum(0, keepdim=False)
    for dim in expanded_dims:
        variable = variable.sum(dim, keepdim=True)
    return variable

def binarize(weight):
    # Use this code for stochastic binarization like in binaryconnect
    weight_temp = torch.clamp((weight + 1) / 2, min=0, max=1)
    weight = (torch.round(weight_temp) * 2) - 1

    # Can be used as ternary, since 0s remain 0s
    weight[weight_temp == 0.5] = 0

    # BUGGY Deterministic binarization (Can be used as ternary, since 0s remain untouched)
    # weight[weight > 0] = 1
    # weight[weight < 0] = -1
    return weight

