import torch
from torch.nn.parameter import Parameter
from torch.nn import Module
from mylinear import mylinear
import math

class BinaryLinear(Module):
    # Inherits from the Module class, check Module class for reference

    def __init__(self, in_features, out_features, bias=True):
#        super(BinaryLinear, self).__init__(in_features, out_features, bias)
        super(BinaryLinear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features

        self.weight = Parameter(torch.Tensor(out_features, in_features))
        self.weightB = torch.Tensor(out_features, in_features)
        self.weightOrg = torch.Tensor(out_features, in_features)  # Temporary storage of weights real values
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)

        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
#        self.weight.data.uniform_(-stdv, stdv)
        self.weight.data.bernoulli_(0.5).mul_(2).add_(-1).div_(99)
        bernoulli_dist = torch.FloatTensor().new().resize_as_(self.weight.data)
        bernoulli_dist.bernoulli_(1)
        self.weight.data = self.weight.data * bernoulli_dist
        if self.bias is not None:
#            self.bias.data.uniform_(-stdv, stdv)
            self.bias.data.uniform_(0, 0)

    def forward(self, input):
#        self.weightB = self.binarized(self.training)    # Is it necessary to return it at all?
#        self.weight = Parameter(self.weightB.clone())
        output = mylinear(self.training, input, self.weight, self.bias)
#        output = super(BinaryLinear, self).forward(input)
#        self.weight = Parameter(self.weightOrg.clone())
        return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
            + str(self.in_features) + ' -> ' \
            + str(self.out_features) + ')'

    def binarized(self, trainFlag):
        self.weightOrg = self.weight.clone().data
        self.weightB = torch.clamp((self.weight.data + 1) / 2, min=0, max=1)
        self.weightB = (torch.round(self.weightB) * 2) - 1
        return self.weightB