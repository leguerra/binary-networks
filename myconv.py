import torch
from torch.autograd.function import InplaceFunction
from torch.autograd import Variable

def myconv(input, weight, bias=None):
    if input is not None and input.dim() != 4:
        raise ValueError("Expected 4D tensor as input, got {}D tensor instead.".format(input.dim()))

    return myConv2d.apply(input, weight, bias)

class myConv2d(InplaceFunction):

    @staticmethod
    def forward(ctx, X, Weights, B):
        '''
        Arguments:
        X -- output activations of the previous layer, tensor of shape (N, C_in, H_prev, W_prev)
        W -- Weights, tensor of size (C_out = filters, C_in, f, f)
        B -- Bias, tensor of size (C_out)

        Returns:
        output -- conv output, tensor of size (N, C_out, H_curr, W_curr)
        '''

        ctx.save_for_backward(X, Weights, B)

        # Retrieving dimensions from X's shape
        (N, C_in, H_prev, W_prev) = X.size()

        # Retrieving dimensions from Weights's shape
        (C_out, C_in, f, f) = Weights.size()

        # Compute the output dimensions assuming no padding and stride = 1
        H_curr = H_prev - f + 1
        W_curr = W_prev - f + 1

        # Initialize the output H with zeros
        output = torch.cuda.FloatTensor(N, C_out, H_curr, W_curr).fill_(0)

        # Looping over samples(n), filters(c_out), input channels(c_in), vertical(h) and horizontal(w) axis of output volume
        for n in xrange(N):
            for c_out in xrange(C_out):
                for h in xrange(H_curr):
                    for w in xrange(W_curr):
                        x_slice = X[n, :, h:h + f, w:w + f]
                        w_slice = Weights[c_out, :, :, :]
                        if (B == None):
                            output[n, c_out, h, w] = torch.sum(x_slice * w_slice)
                        else:
                            output[n, c_out, h, w] = torch.sum(x_slice * w_slice) + B[c_out]

        return output

    @staticmethod
    def backward(ctx, grad_output):
        '''
        Arguments:
        grad_output -- gradient of the cost with respect to output, numpy array of shape (N, C_out, H, W)

        Returns:
        grad_X -- gradient with respect to input, tensor of shape (N, C_in, H_prev, W_prev)
        grad_W -- gradient with respect to the weights, tensor of shape (C_out, C_in, f, f)
        grad_B -- gradient with respect to the bias, tensor of shape (C_out)
        '''

        X, Weights, B = ctx.saved_variables
        grad_X = grad_W = grad_B = None

        # Although these variables were torchTensors in the forward (this is automatically done by pytorch),
        # they are stored in ctx as pytorch Variables
        X = X.data
        Weights = Weights.data
        if (B != None):
            B = B.data

        # Retrieving dimensions from W's shape
        (C_out, C_in, f, f) = Weights.shape

        # Retrieving dimensions from dH's shape
        (N, C_out, H, W) = grad_output.data.shape

        # Initializing dX, dW with the correct shapes
        grad_X = Variable(torch.cuda.FloatTensor(X.shape).fill_(0))
        grad_W = Variable(torch.cuda.FloatTensor(Weights.shape).fill_(0))
        grad_B = None
        if (B != None):
            grad_B = Variable(torch.cuda.FloatTensor(B.shape).fill_(0))

        # Looping over samples(n), filters(c_out), input channels(c_in), vertical(h) and horizontal(w) axis of output volume
        for n in xrange(N):
            for c_out in xrange(C_out):
                for h in xrange(H):
                    for w in xrange(W):
                        grad_W.data[c_out, :] += X[n, :, h:h + f, w:w + f] * grad_output.data[n, c_out, h, w]
                        grad_X.data[n, :, h:h + f, w:w + f] += Weights[c_out, :] * grad_output.data[n, c_out, h, w]
                        if (B != None):
                            grad_B.data[c_out] += grad_output.data[n, c_out, h, w]

        return grad_X, grad_W, grad_B, None, None, None


def binarize(weight):
    # Use this code for stochastic binarization like in binaryconnect
    weight_temp = torch.clamp((weight + 1) / 2, min=0, max=1)
    weight = (torch.round(weight_temp) * 2) - 1

    # Can be used as ternary, since 0s remain 0s
    weight[weight_temp == 0.5] = 0

    # BUGGY Deterministic binarization (Can be used as ternary, since 0s remain untouched)
    # weight[weight > 0] = 1
    # weight[weight < 0] = -1
    return weight

