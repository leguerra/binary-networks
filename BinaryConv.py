import torch
from torch.nn.parameter import Parameter
from torch.nn.modules import Conv2d
import math
from myconv import myconv

class BinaryConv2d(Conv2d):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True):
        super(BinaryConv2d, self).__init__(in_channels, out_channels, kernel_size, stride,
                 padding, dilation, groups, bias)

        self.weightB = torch.Tensor(self.weight.size())
        self.weightOrg = torch.Tensor(self.weight.size())  # Temporary storage of weights real values

    def reset_parameters(self):
        n = self.in_channels
        for k in self.kernel_size:
            n *= k
        stdv = 1. / math.sqrt(n)
#        self.weight.data.uniform_(-stdv, stdv)
        self.weight.data.bernoulli_(0.5).mul_(2).add_(-1).div_(100)
        bernoulli_dist = torch.FloatTensor().new().resize_as_(self.weight.data)
        bernoulli_dist.bernoulli_(1)
        self.weight.data = self.weight.data * bernoulli_dist

        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input):
        self.weightOrg = self.weight.clone().data
        self.binarized(self.training)    # Is it necessary to return it at all?
        self.weight.data = self.weightB
        output = super(BinaryConv2d, self).forward(input)
#        self.weight.data = self.weightOrg.clone()

        # output = myconv(input, self.weight, self.bias)
        return output

    def binarized(self, trainFlag):
        matrix_temp = torch.clamp((self.weight.data + 1) / 2, min=0, max=1)
        self.weightB = (torch.round(matrix_temp) * 2) - 1
        self.weightB[matrix_temp == 0.5] = 0
        return
