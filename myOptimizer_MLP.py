from torch.optim.optimizer import Optimizer, required
from torch import sort, sum, max, min, rand, sign, cuda, pow, mean, std
import math

class myOptimizer(Optimizer):
    """Based on stochastic gradient descent class (actually I should inherit from SGD instead)
    """

    def __init__(self, params, lr=required, momentum=0, dampening=0,
                 weight_decay=0, nesterov=False):
        defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
                        weight_decay=weight_decay, nesterov=nesterov)
        if nesterov and (momentum <= 0 or dampening != 0):
            raise ValueError("Nesterov momentum requires a momentum and zero dampening")
        super(myOptimizer, self).__init__(params, defaults)
        self.method = "stochastic"             # "real_weights", "hard", "stochastic"
        self.print_count = False
        self.ternary = False
        if self.ternary:
            self.spa = 0.2  # Sparsity
        else:
            self.spa = 0

    def __setstate__(self, state):
        super(myOptimizer, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('nesterov', False)

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            weight_decay = group['weight_decay']
            momentum = group['momentum']
            dampening = group['dampening']
            nesterov = group['nesterov']

            for p_index, p in enumerate(group['params']):
                if p.grad is None:
                    continue
                d_p = p.grad.data
                if weight_decay != 0:
                    d_p.add_(weight_decay, p.data)
                if momentum != 0:
                    param_state = self.state[p]
                    if 'momentum_buffer' not in param_state:
                        buf = param_state['momentum_buffer'] = d_p.clone()
                    else:
                        buf = param_state['momentum_buffer']
                        buf.mul_(momentum).add_(1 - dampening, d_p)
                    if nesterov:
                        d_p = d_p.add(momentum, buf)
                    else:
                        d_p = buf

                if 0:
                    print(
                        'p_index: {}\t weights [{:3f}|{:3f}] dif {} zero {}\t gradients [{:3f}|{:3f}]\t'.format(
                            p_index,
                            mean(p.data), std(p.data), (sum(p.data > 0) - sum(p.data < 0)), sum(p.data == 0),
                            mean(d_p), std(d_p)
                        ))

                if ((p_index == 0) or (p_index == 4) or (p_index == 8) or (p_index == 12)) and (self.method == "stochastic"):
                    max_val = max(d_p)
                    min_val = min(d_p)

                    d_p[d_p > 0] = d_p[d_p > 0].div(max_val)
                    d_p[d_p < 0] = d_p[d_p < 0].div(abs(min_val))

#                    rand_p_left = cuda.FloatTensor(p.data.size()).exponential_(group['lr'] + mean(d_p))
#                    rand_p_right = cuda.FloatTensor(p.data.size()).exponential_(group['lr'] + mean(d_p))

                    rand_p = cuda.FloatTensor(p.data.size()).exponential_(14)              # Use with batch size larger than 1
#                    rand_p = cuda.FloatTensor(p.data.size()).bernoulli_(.0005)              # Use with batch size 1

                    temp_d_p = d_p.clone()
#                    temp_d_p[(p.data > 0) & (d_p < 0)] = .01
                    temp_d_p[(p.data > 0) & (d_p > 0)] = (1 - temp_d_p[(p.data > 0) & (d_p > 0)]) - rand_p[(p.data > 0) & (d_p > 0)] - self.spa
                    temp_d_p[(p.data < 0) & (d_p < 0)] = (-1 - temp_d_p[(p.data < 0) & (d_p < 0)]) + rand_p[(p.data < 0) & (d_p < 0)] + self.spa
                    temp_d_p[(p.data == 0) & (d_p > 0)] = (1 - temp_d_p[(p.data == 0) & (d_p > 0)]) - rand_p[(p.data == 0) & (d_p > 0)] + self.spa
                    temp_d_p[(p.data == 0) & (d_p < 0)] = (-1 - temp_d_p[(p.data == 0) & (d_p < 0)]) + rand_p[(p.data == 0) & (d_p < 0)] - self.spa
#                    temp_d_p[(p.data < 0) & (d_p > 0)] = -.01

                    if self.print_count:
                        print(
                        'p_index: {}\t no change [{:3d}|{:3d}]\t change [{:3d}|{:3d}]\t'.format(
                            p_index,
                            sum((temp_d_p[(p.data > 0) & (d_p < 0)]) > 0),
                            sum((temp_d_p[(p.data < 0) & (d_p > 0)]) < 0),
                            sum((temp_d_p[(p.data > 0) & (d_p > 0)]) < 0),
                            sum((temp_d_p[(p.data < 0) & (d_p < 0)]) > 0),
                        ))

#                    p.data = temp_d_p
                    if self.ternary:
                        p.data[(p.data > 0) & (d_p > 0) & (temp_d_p < 0)] = 0
                        p.data[(p.data == 0) & (d_p > 0) & (temp_d_p < 0)] = -1
                        p.data[(p.data == 0) & (d_p < 0) & (temp_d_p > 0)] = 1
                        p.data[(p.data < 0) & (d_p < 0) & (temp_d_p > 0)] = 0
                    else:
#                        p.data[(p.data > 0) & (d_p > 0) & (temp_d_p < 0)] = temp_d_p[(p.data > 0) & (d_p > 0) & (temp_d_p < 0)]
#                        p.data[(p.data < 0) & (d_p < 0) & (temp_d_p > 0)] = temp_d_p[(p.data < 0) & (d_p < 0) & (temp_d_p > 0)]
                        p.data[(p.data > 0) & (d_p > 0) & (temp_d_p < 0)] = -1
                        p.data[(p.data < 0) & (d_p < 0) & (temp_d_p > 0)] = 1

                elif ((p_index == 0) or (p_index == 4) or (p_index == 8)) and (self.method == "hard"):
                    thresh_id_low = int((d_p.size()[0] * d_p.size()[1])*.0005)
                    thresh_id_high = int((d_p.size()[0] * d_p.size()[1]) * .9995)
                    thresh_val_low = sort(d_p.view(1, -1))[0][0][thresh_id_low]
                    thresh_val_high = sort(d_p.view(1, -1))[0][0][thresh_id_high]

                    if (thresh_val_low != 0) and (thresh_val_high != 0):
                        if self.print_count:
                            print('p_index: {}\t sleeping [{:3d}|{:3d}]\t no change [{:3d}|{:3d}]\t awaking [{:3d}|{:3d}]'.format(
                                p_index,
                                sum((p.data > 0) & (d_p >= (thresh_val_high - (thresh_val_high * self.spa)))), sum((p.data < 0) & (d_p <= (thresh_val_low - (thresh_val_low * self.spa)))),
                                sum((p.data > 0) & (d_p <= (thresh_val_low + (thresh_val_low * self.spa)))), sum((p.data < 0) & (d_p >= (thresh_val_high + (thresh_val_high * self.spa)))),
                                sum((p.data == 0) & (d_p <= (thresh_val_low + (thresh_val_low * self.spa)))), sum((p.data == 0) & (d_p >= (thresh_val_high + (thresh_val_high * self.spa)))),
                            ))

                        temp_p = p.data.clone()
                        if self.ternary:
                            temp_p[(p.data > 0) & (d_p <= (thresh_val_low + (thresh_val_low * self.spa)))] = 1
                            temp_p[(p.data < 0) & (d_p <= (thresh_val_low - (thresh_val_low * self.spa)))] = 0
                            temp_p[(p.data == 0) & (d_p <= (thresh_val_low + (thresh_val_low * self.spa)))] = 1
                            temp_p[(p.data == 0) & (d_p >= (thresh_val_high + (thresh_val_high * self.spa)))] = -1
                            temp_p[(p.data > 0) & (d_p >= (thresh_val_high - (thresh_val_high * self.spa)))] = 0
                            temp_p[(p.data < 0) & (d_p >= (thresh_val_high + (thresh_val_high * self.spa)))] = -1
                        else:
                            temp_p[(p.data > 0) & (d_p <= thresh_val_low)] = 1
                            temp_p[(p.data < 0) & (d_p <= thresh_val_low)] = 1
                            temp_p[(p.data > 0) & (d_p >= thresh_val_high)] = -1
                            temp_p[(p.data < 0) & (d_p >= thresh_val_high)] = -1

                        p.data = temp_p

                else:
                    lr = 0.01
                    p.data.add_(-lr, d_p)

        return loss

