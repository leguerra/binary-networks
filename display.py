import matplotlib.pyplot as plt
import numpy as np
import torch
import pdb

def print_gradients(self, grad_input, grad_output):
    print('Inside ' + self.__class__.__name__ + ' backward')
    print('Inside class:' + self.__class__.__name__)
    print('')
    print('grad_input: ', type(grad_input))
    print('grad_input[0]: ', grad_input[0])     # wrt input
    print('grad_input[1]: ', grad_input[1])     # wrt weights
    print('grad_input[2]: ', grad_input[2])     # wrt bias
    print('grad_output[0]: ', grad_output[0])
    return

def print_forward(self, input, output):
    print('Inside ' + self.__class__.__name__ + ' backward')
    print('Inside class:' + self.__class__.__name__)
    print('')
    print('Input: ', input)
    print('Output: ', output)
    print('')
    return

def plot_weights(model, axarr):
    axarr[0, 0].clear()
    axarr[0, 1].clear()
    axarr[1, 0].clear()
    axarr[1, 1].clear()
    #            axarr[1, 1].set_xlim([-.5, .5])
    #            axarr[1, 1].set_ylim([0, 400])

    a = model.fc1.weight.grad.data.numpy()
    a = np.reshape(a, (1, -1))
    axarr[0, 0].plot(a[0])
    plt.pause(0.001)

    #           a = binarized(model.fc4.weight[:, :]).data.numpy()
    a = model.fc3.weight.grad[:, :].data.numpy()
    a = np.reshape(a, (1, -1))
    axarr[0, 1].plot(a[0])
    #            axarr[0, 1].hist(a[0], bins='auto')
    plt.pause(0.001)

    a = model.fc3.weight[:, :].data.numpy()
    a = np.reshape(a, (1, -1))
    #            axarr[1, 0].plot(a[0])
    plt.pause(0.001)

    a = model.fc4.weight.grad.data.numpy()
    #            a = model.fc4.weight[:, :].data.numpy()
    a = np.reshape(a, (1, -1))
    axarr[1, 1].plot(a[0])
    #            axarr[1, 1].hist(a[0], bins='auto')
    plt.pause(0.001)

    return